import React from "react";

export const About = () => {
  return (
    <div>
      <h1>About US
      </h1>
      <p> Empowering Digital Transformation

· At GT Global Solutions, we are passionate professionals committed to transforming businesses through cutting-edge IT solutions. Explore our journey, mission, and values.

Description : Empowering Digital Transformation. At GT Global Solutions, we are passionate professionals committed to transforming businesses through cutting-edge IT solutions. Our journey is marked by a dedication to innovation, a clear mission to empower organizations digitally, and unwavering values that guide our approach to client partnerships.</p>
    </div>
  );
};
