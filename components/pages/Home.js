import React from "react";

export const Home = () => {
  return (
    <div>
      <h1>Home</h1>
      <p>Welcome to GT Global Solutions, where innovation meets expertise. Elevate your business with our customized IT solutions designed to drive success in the digital era.</p>
    </div>
  );
};
