import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";
import {  HamburgetMenuClose, HamburgetMenuOpen } from "./Icons";

function NavBar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  return (
    <>
      <nav className="navbar">
        <div className="nav-container">
          <NavLink exact to="/" className="nav-logo">
            <span>GT GLOBAL SOLUTIONS</span>
            {/* <i className="fas fa-code"></i> */}
            <span className="icon">
            
            </span>
          </NavLink>

          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <NavLink
                exact
                to="/"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/about"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                About
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/services"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Services
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/solutions"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Solutions
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/industries"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Industries Served
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/case studies"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Case Studies
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                exact
                to="/blog"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Blog
              </NavLink>
            </li>
            
            <li className="nav-item">
              <NavLink
                exact
                to="/contact"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Contact Us
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/get"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Get a Quote
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/client"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Client Testimonals
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/careers"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Careers
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/faqs"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                FAQS
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/partner"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Partnership
              </NavLink>
            </li>
          </ul>
          <div className="nav-icon" onClick={handleClick}>
            {/* <i className={click ? "fas fa-times" : "fas fa-bars"}></i> */}

            {click ? (
              <span className="icon">
                <HamburgetMenuOpen />{" "}
              </span>
            ) : (
              <span className="icon">
                <HamburgetMenuClose />
              </span>
            )}
          </div>
        </div>
      </nav>
    </>
  );
}

export default NavBar;
